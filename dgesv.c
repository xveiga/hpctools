#include "dgesv.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifndef ALIGNMENT_SIZE
#define ALIGNMENT_SIZE 64
#endif

// #define FUNC_OPT // Optimized memcpy functions and aligned memory
// #define CC_OPT // Compiler pragma indications
// typedef double double_aligned __attribute__((aligned(ALIGNMENT_SIZE)));

// Prints a matrix for debug
void mprint(char *tag, int m, int n, double *a)
{
	int i, j;
	printf("\n%s\n", tag);
	for (i = 0; i < m; i++)
	{
		for (j = 0; j < n; j++) {
			printf("\t%3.2f", a[i * m + j]);
		}
		printf("\n");
	}
}

int my_dgesv(int n, int nrhs, double *restrict a, double *restrict b)
{
	/* Gausssian elimination method
	 * The original algorithm reduces the input matrix into row-echelon form
	 * in order to obtain the solutions.
	 *
	 * In order to understand how the input and output parameters should work,
	 * Intel has a very descriptive dgesv example:
	 * https://www.intel.com/content/www/us/en/docs/onemkl/code-samples-lapack/2023-1/dgesv-example-c.html
	 *
	 * By taking a look at the original fortran lapack implementation
	 * (https://github.com/Reference-LAPACK/lapack/blob/master/SRC/dgesv.f),
	 * we see it does this in two distinct steps, LU factorization, in order
	 * to obtain a triangular matrix, and then actually solving the system,
	 * overwriting the values of B with those of X.
	 *
	 * By reading some tutorials and example implementations, I've come up with
	 * this naïve version. I did not fully understand how to implement the
	 * algorithm at first, so some parts are just taken from tutorials, and
	 * modified in order to work properly. Probably there's better ways to do
	 * this, specially in regards to memory usage. The execution time is sadly
	 * orders of magnitude bigger than the reference dgesv implementation.
	 * For now, this is all I could do.
	 */

	// Create temporary intermediate matrixes
	int mstride = (n+1);

#ifdef FUNC_OPT
	// https://stackoverflow.com/questions/53783149/does-calloc-of-a-double-field-always-evaluate-to-0-0
	// double_aligned *restrict x = calloc(n * n, sizeof(double_aligned));
	// double_aligned *restrict m = malloc(n * mstride * sizeof(double_aligned));
	// The aligned_alloc function is only available from C11 onwards. Best to use posix_memalign on POSIX systems.
	// double_aligned *restrict x = aligned_alloc(ALIGNMENT_SIZE, n * n * sizeof(double_aligned));
	// double_aligned *restrict m = aligned_alloc(ALIGNMENT_SIZE, n * mstride * sizeof(double_aligned));

	double *restrict x, *restrict m;
	if (posix_memalign((void *) &x, ALIGNMENT_SIZE, n * n * sizeof(double)) ||
		posix_memalign((void *) &m, ALIGNMENT_SIZE, n * mstride * sizeof(double))) {
		return -1;
	}
	// No calloc exists for aligned memory in the C standard, so we have to use memset
	memset(x, 0, n * n * sizeof(double));

#else
	double *x = malloc(n*n*sizeof(double));
	double *m = malloc(n * mstride * sizeof(double));
	#pragma ivdep
	for (int i=0; i<n * n; i++) {
		x[i] = 0;
	}
#endif

	// Needed for OMP parallelization
	int col, i, j, k;
	double c, sum;

// #pragma omp parallel private(col)
{
		// Iterate over each column of A
		for (col = 0; col < n; col++)
		{
				// Move data to temporary matrix
#pragma omp parallel for private(j) schedule(static) // Default is already static, but ensure so
// #pragma omp simd
#ifdef CC_OPT
#pragma gcc ivdep // For GCC
#pragma ivdep // For ICX
#endif
		for (i = 0; i < n; i++)
		{
#ifdef FUNC_OPT
			// Copy keeping stride of +1 in mind
			memcpy(&m[i * mstride], &a[i * n], n*sizeof(double));
#else
			for (j=0; j<n; j++) {
				m[i*mstride+j] = a[i*n+j];
			}
#endif

			// Store the values of B in the temporary matrix as well
			m[i * mstride + n] = b[i * n + col];
		}

		// Upper triangular matrix forward substitution
		// Not vectorized because of nested loops
		// TODO: Collapse this for loop by hand
// #pragma omp parallel for private(i, c, k) schedule(static)
		for (j = 0; j < n; j++) {
			for (i = j+1; i < n; i++) {
					c = m[i * mstride + j] / m[j * mstride + j];
// #pragma omp simd
#ifdef CC_OPT
#pragma gcc ivdep // GCC
#pragma ivdep // ICX
#endif
					for (k = 0; k <= n; k++) {
						m[i * mstride + k] -= c * m[j * mstride + k];
					}
			}
		}

		x[(n - 1) * n + col] = m[(n - 1) * mstride + n] / m[(n - 1) * mstride + (n - 1)];

		// backward substitution
// #pragma omp parallel for private(j, col, sum) schedule(static)
		for (i = n - 2; i >= 0; i--) {
			sum = 0;
// #pragma omp simd
// #ifdef CC_OPT
// #pragma gcc ivdep // GCC
// #pragma ivdep // ICX
// #endif
#pragma omp parallel for reduction(+:sum) schedule(static)
				for (j = i; j < n; j++)
				{
					sum = sum + m[i * mstride + j] * x[j * n + col];
				}
				x[i * n + col] = (m[i * mstride + n] - sum) / m[i * mstride + i];
			}
		}

		// Copy back solution to B.
#ifdef FUNC_OPT
// #pragma omp single
// {
	// Try with optimized memcopy
	memcpy(b, x, n*n*sizeof(double));
// }
#else

#ifdef CC_OPT
#pragma gcc ivdep
#pragma ivdep
#endif
// #pragma omp simd
#pragma omp parallel for schedule(static)
	for (int i=0; i<n*n; i++)
		b[i] = x[i];

#endif

}
	// Free temporary buffers
	free(x);
	free(m);
	return 0;
}
