#!/bin/bash

#SBATCH -J dgesv
#SBATCH -t 10:00
#SBATCH --mem 2GB
#SBATCH -c 16
#SBATCH -o "%x-%j.out"
#SBATCH -e "%x-%j.err"

# Benchmark every size up to 512, just until it takes too long
TEST_SIZES="32 64 128 256 512"

# Benchmark gcc
# module --force purge
# module load cesga/2020 gcc openblas
# # Benchmark every optimization option
# for opt in "-O0" "-O1" "-O2" "-O3" "-Ofast"; do
#     echo "############################"
#     echo "# GCC Optimization level: ${opt}"
#     make clean
#     CC=gcc CFLAGS="${opt}" make -j$(nproc)
#     # Benchmark every size up to 512, just until it takes too long
#     for size in $TEST_SIZES; do
#         echo "%%%%%%%%%%%%%%%"
#         echo "% Size: ${size}"
#         ./dgesv "${size}"
#         echo "%%%%%%%%%%%%%%%"
#     done
#     echo "############################"
# done

# # Benchmark icc
# module --force purge
# module load cesga/2020 intel imkl
# # Benchmark every optimization option
# for opt in "-O0" "-O1" "-O2" "-O3" "-Ofast"; do
#     echo "############################"
#     echo "# ICX Optimization level: ${opt}"
#     make clean
#     CC=icx CFLAGS="${opt}" make -j$(nproc)
#     # Benchmark every size up to 512, just until it takes too long
#     for size in $TEST_SIZES; do
#         echo "%%%%%%%%%%%%%%%"
#         echo "% Size: ${size}"
#         ./dgesv "${size}"
#         echo "%%%%%%%%%%%%%%%"
#     done
#     echo "############################"
# done

# Benchmark non-optimized version
module --force purge
module load cesga/2020 gcc openblas
echo "############################"
echo "# GCC non-optimized"
make clean
CC=gcc CFLAGS="-O0" make -j$(nproc)
for size in $TEST_SIZES; do
    ./dgesv "${size}"
done
echo "############################"


# Benchmark icc
module --force purge
module load cesga/2020 intel imkl
echo "############################"
echo "# ICX non-optimized"
make clean
CC=icx CFLAGS="-O0" make -j$(nproc)
for size in $TEST_SIZES; do
    ./dgesv "${size}"
done
echo "############################"



# Benchmark all compiler optimization enabled (including manual ones)
module --force purge
module load cesga/2020 gcc openblas
echo "############################"
echo "# GCC optimized"
make clean
CC=gcc CFLAGS="-O3 -DCC_OPT" make -j$(nproc)
for size in $TEST_SIZES; do
    ./dgesv "${size}"
done
echo "############################"


# Benchmark icc
module --force purge
module load cesga/2020 intel imkl
echo "############################"
echo "# ICX optimized"
make clean
CC=icx CFLAGS="-O3 -DCC_OPT" make -j$(nproc)
for size in $TEST_SIZES; do
    ./dgesv "${size}"
done
echo "############################"


# Benchmark compiler optimizations, plus memory optimized functions, plus aligned memory.
module --force purge
module load cesga/2020 gcc openblas
echo "############################"
echo "# GCC all optimizations"
make clean
CC=gcc CFLAGS="-march=native -mtune=native -O3 -DCC_OPT -DFUNC_OPT" make -j$(nproc)
for size in $TEST_SIZES; do

    ./dgesv "${size}"
done
echo "############################"

# Benchmark icc
module --force purge
module load cesga/2020 intel imkl
echo "############################"
echo "# ICX all optimizations"
make clean
CC=icx CFLAGS="-march=native -mtune=native -O3 -DCC_OPT -DFUNC_OPT" make -j$(nproc)
for size in $TEST_SIZES; do
    ./dgesv "${size}"
done
echo "############################"

# Benchmark compiler optimizations, plus memory optimized functions, plus aligned memory, plus OpenMP
module --force purge
module load cesga/2020 gcc openblas
echo "############################"
echo "# GCC non-optimized"
make clean
CC=gcc OMP_OPT=1 CFLAGS="-march=native -mtune=native -O3 -DCC_OPT -DFUNC_OPT" make -j$(nproc)
for size in $TEST_SIZES; do

    ./dgesv "${size}"
done
echo "############################"

# Benchmark icc
module --force purge
module load cesga/2020 intel imkl
echo "############################"
echo "# ICX non-optimized"
make clean
CC=icx OMP_OPT=1 CFLAGS="-march=native -mtune=native -O3 -DCC_OPT -DFUNC_OPT" make -j$(nproc)
for size in $TEST_SIZES; do
    ./dgesv "${size}"
done
echo "############################"