# Default Lapacke: Openblas at CESGA
# LDLIBS=-lopenblas

# Other systems (my Debian boxes, for example)
#LDLIBS=-llapacke

# Intel MKL at CESGA
# Module needed: imkl
# => module load openblas
# LDLIBS for intel compiler: icx (module needed: intel)
# Just invoke make like this: make CC=icx
# LDLIBS=-qmkl=sequential -lmkl_intel_lp64

CFLAGS ?= -O3

# Debug
DEBUG ?= 0
ifeq ($(DEBUG),1)
	CFLAGS += -g3
$(info DEBUG configuration enabled)
endif

# Different libraries for gcc/icx. Default gcc
ifeq ($(CC),icx)
$(info Using Intel MKL)
	LDLIBS=-qmkl=sequential -lmkl_intel_lp64
	CFLAGS += -DIMKL
else
$(info Using OpenBLAS)
	LDLIBS=-lopenblas
	CFLAGS += -DOPENBLAS
endif

ifeq ($(OMP_OPT),1)
$(info OPENMP enabled)
	CFLAGS += -fopenmp -DOMP_OPT
endif

dgesv: dgesv.o timer.o main.o
	$(CC) $(CFLAGS) -o $@ $+ $(LDLIBS)

clean:
	$(RM) dgesv *.o *~
