# Task #3: Software profiling and optimization

Xián García Veiga (x.veiga@udc.es), December 2023

## Introduction
This task consists of performing software profiling and optimizing the code written previously on Task 1 of this subject. The initial implementation was pieced together copying different snippets from different sources. As I had no previous experience using linear algebra functions at this scale, let alone implementing them, I wanted to get an in-depth look at how the original function works and how it is used. _Intel_ has a pretty comprehensive [tutorial](https://www.intel.com/content/www/us/en/docs/onemkl/code-samples-lapack/2023-1/dgesv-example-c.html) on how to use the function provided by their Math Kernel Library. I took a look as well at the reference implementation from [_LAPACK_] (https://github.com/Reference-LAPACK/lapack/blob/master/SRC/dgesv.f), but found it too complicated and time consuming to follow. I ended up piecing together the code for the previous task without fully understanding what it did, and as a consequence it had memory errors, which I'll try to fix on this **Task 3**.

## Tasks

### Task 1
_Check your serial implementation with Valgrind Memcheck to find possible memory issues. Have you found any? Which ones? Solve them and get a clean Valgrind run._

Yes, upon running _memcheck_, the code has multiple memory leaks, and suffers from memory access errors. At first I expected my implementation to be the culprit, however, valgrind points at the main function in the program, specifically on the `generate_matrix` calls. It must be noted that for valgrind to report the location of the memory problems, it must be compiled with `gcc`-s debugging option `-g`.
```shell
make && valgrind --tool=memcheck --leak-check=full --show-leak-kinds=all ./dgesv 128
```
```
cc -O3 -g   -c -o dgesv.o dgesv.c
cc -O3 -g   -c -o main.o main.c
cc   dgesv.o timer.o main.o  -lopenblas -o dgesv
==507197== Memcheck, a memory error detector
==507197== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==507197== Using Valgrind-3.15.0 and LibVEX; rerun with -h for copyright info
==507197== Command: ./dgesv 128
==507197== 
Time taken by Lapacke dgesv: 458 ms
Time taken by my dgesv solver: 3842 ms
==507197== Conditional jump or move depends on uninitialised value(s)
==507197==    at 0x109279: check_result (main.c:47)
==507197==    by 0x109279: main (main.c:98)
==507197== 
Result is ok!
==507197== 
==507197== HEAP SUMMARY:
==507197==     in use at exit: 524,808 bytes in 6 blocks
==507197==   total heap usage: 34 allocs, 28 frees, 1,261,432 bytes allocated
==507197== 
==507197== 8 bytes in 1 blocks are still reachable in loss record 1 of 6
==507197==    at 0x483779F: malloc (in /mnt/netapp1/Optcesga_FT2_RHEL7/2020/gentoo/22072020/usr/lib64/valgrind/vgpreload_memcheck-amd64-linux.so)
==507197==    by 0x5BDC7BC: ??? (in /mnt/netapp1/Optcesga_FT2_RHEL7/2020/gentoo/22072020/usr/lib64/gcc/x86_64-pc-linux-gnu/11.2.0/libgomp.so.1.0.0)
==507197==    by 0x5BEDCEA: ??? (in /mnt/netapp1/Optcesga_FT2_RHEL7/2020/gentoo/22072020/usr/lib64/gcc/x86_64-pc-linux-gnu/11.2.0/libgomp.so.1.0.0)
==507197==    by 0x5BDA7D4: ??? (in /mnt/netapp1/Optcesga_FT2_RHEL7/2020/gentoo/22072020/usr/lib64/gcc/x86_64-pc-linux-gnu/11.2.0/libgomp.so.1.0.0)
==507197==    by 0x400FD51: ??? (in /mnt/netapp1/Optcesga_FT2_RHEL7/2020/gentoo/22072020/lib64/ld-2.31.so)
==507197==    by 0x400FE58: ??? (in /mnt/netapp1/Optcesga_FT2_RHEL7/2020/gentoo/22072020/lib64/ld-2.31.so)
==507197==    by 0x40010C9: ??? (in /mnt/netapp1/Optcesga_FT2_RHEL7/2020/gentoo/22072020/lib64/ld-2.31.so)
==507197==    by 0x1: ???
==507197==    by 0x1FFEFFDB3E: ???
==507197==    by 0x1FFEFFDB46: ???
==507197== 
==507197== 512 bytes in 1 blocks are definitely lost in loss record 2 of 6
==507197==    at 0x483779F: malloc (in /mnt/netapp1/Optcesga_FT2_RHEL7/2020/gentoo/22072020/usr/lib64/valgrind/vgpreload_memcheck-amd64-linux.so)
==507197==    by 0x109182: main (main.c:77)
==507197== 
==507197== 131,072 bytes in 1 blocks are definitely lost in loss record 3 of 6
==507197==    at 0x483779F: malloc (in /mnt/netapp1/Optcesga_FT2_RHEL7/2020/gentoo/22072020/usr/lib64/valgrind/vgpreload_memcheck-amd64-linux.so)
==507197==    by 0x109AEA: generate_matrix (main.c:15)
==507197==    by 0x10914A: main (main.c:67)
==507197== 
==507197== 131,072 bytes in 1 blocks are definitely lost in loss record 4 of 6
==507197==    at 0x483779F: malloc (in /mnt/netapp1/Optcesga_FT2_RHEL7/2020/gentoo/22072020/usr/lib64/valgrind/vgpreload_memcheck-amd64-linux.so)
==507197==    by 0x109AEA: generate_matrix (main.c:15)
==507197==    by 0x109159: main (main.c:68)
==507197== 
==507197== 131,072 bytes in 1 blocks are definitely lost in loss record 5 of 6
==507197==    at 0x483779F: malloc (in /mnt/netapp1/Optcesga_FT2_RHEL7/2020/gentoo/22072020/usr/lib64/valgrind/vgpreload_memcheck-amd64-linux.so)
==507197==    by 0x109B5B: duplicate_matrix (main.c:28)
==507197==    by 0x109166: main (main.c:69)
==507197== 
==507197== 131,072 bytes in 1 blocks are definitely lost in loss record 6 of 6
==507197==    at 0x483779F: malloc (in /mnt/netapp1/Optcesga_FT2_RHEL7/2020/gentoo/22072020/usr/lib64/valgrind/vgpreload_memcheck-amd64-linux.so)
==507197==    by 0x109B5B: duplicate_matrix (main.c:28)
==507197==    by 0x109173: main (main.c:70)
==507197== 
==507197== LEAK SUMMARY:
==507197==    definitely lost: 524,800 bytes in 5 blocks
==507197==    indirectly lost: 0 bytes in 0 blocks
==507197==      possibly lost: 0 bytes in 0 blocks
==507197==    still reachable: 8 bytes in 1 blocks
==507197==         suppressed: 0 bytes in 0 blocks
==507197== 
==507197== Use --track-origins=yes to see where uninitialised values come from
==507197== For lists of detected and suppressed errors, rerun with: -s
==507197== ERROR SUMMARY: 16261 errors from 6 contexts (suppressed: 13 from 6)
```

Upon closer investigation, we can see that the matrix duplication allocates memory via the `duplicate_matrix` function, which has a `malloc` call inside, but no calls to `free`. Introducing memory allocation calls this way, inside a function can lead to errors of this kind. It's usually preferred to put the `malloc` and `free` calls on the same level whenever possible, indicate this on the function name or to leave this responsibility to the caller. The main was also missing a `free` for the `ipiv` argument as well. I add the corresponding frees at the end of the main function as such:
```c
int main(int argc, char *argv[])
{
    a = generate_matrix(size, 1);
    b = generate_matrix(size, 2);
    aref = duplicate_matrix(a, size);
    bref = duplicate_matrix(b, size);
    ...
    ...
    ...
    free(ipiv);
    free(a);
    free(b);
    free(aref);
    free(bref);

    return 0;
}
```

When running valgrind, it still reports 8 bytes as still reachable. They do not report any specific information, as they are part of a library. They are a common error and arguably not a major problem, but unfortunately we cannot get rid of them and get a fully clean `memcheck` run. Upon investigating, If the `dgesv` function call is removed, and avoid linking with _openblas_, this error does not appear anymore.
```shell
==510069== 8 bytes in 1 blocks are still reachable in loss record 1 of 1
==510069==    at 0x483779F: malloc (in /mnt/netapp1/Optcesga_FT2_RHEL7/2020/gentoo/22072020/usr/lib64/valgrind/vgpreload_memcheck-amd64-linux.so)
==510069==    by 0x5BDC7BC: ??? (in /mnt/netapp1/Optcesga_FT2_RHEL7/2020/gentoo/22072020/usr/lib64/gcc/x86_64-pc-linux-gnu/11.2.0/libgomp.so.1.0.0)
==510069==    by 0x5BEDCEA: ??? (in /mnt/netapp1/Optcesga_FT2_RHEL7/2020/gentoo/22072020/usr/lib64/gcc/x86_64-pc-linux-gnu/11.2.0/libgomp.so.1.0.0)
==510069==    by 0x5BDA7D4: ??? (in /mnt/netapp1/Optcesga_FT2_RHEL7/2020/gentoo/22072020/usr/lib64/gcc/x86_64-pc-linux-gnu/11.2.0/libgomp.so.1.0.0)
==510069==    by 0x400FD51: ??? (in /mnt/netapp1/Optcesga_FT2_RHEL7/2020/gentoo/22072020/lib64/ld-2.31.so)
==510069==    by 0x400FE58: ??? (in /mnt/netapp1/Optcesga_FT2_RHEL7/2020/gentoo/22072020/lib64/ld-2.31.so)
==510069==    by 0x40010C9: ??? (in /mnt/netapp1/Optcesga_FT2_RHEL7/2020/gentoo/22072020/lib64/ld-2.31.so)
==510069==    by 0x1: ???
==510069==    by 0x1FFEFFDB3E: ???
==510069==    by 0x1FFEFFDB46: ???
==510069==
==510069== LEAK SUMMARY:
==510069==    definitely lost: 0 bytes in 0 blocks
==510069==    indirectly lost: 0 bytes in 0 blocks
==510069==      possibly lost: 0 bytes in 0 blocks
==510069==    still reachable: 8 bytes in 1 blocks
==510069==         suppressed: 0 bytes in 0 blocks
```

The initial valgrind check also reported the following:
```shell
==507197== Conditional jump or move depends on uninitialised value(s)
==507197==    at 0x109279: check_result (main.c:47)
==507197==    by 0x109279: main (main.c:98)
```
This was caused by the use of `malloc` on the `dgesv.c` implementation, which may return unintialized values. Replacing it with `calloc` solved this warning (Using calloc to initialize floating point values is valid, as it sets all the bits to zero). Also, the size of the `x` buffer was improperly calculated:
```c
// double *x = malloc(2*n*n*sizeof(double));
double *x = calloc(n*n, sizeof(double));
```

These steps are on a separate commit tagged `task3-step1` on the repository.

### Task 2
_Basic benchmarking of your dgesv implementation (Task #1, after possible modifications introduced in step #0)_

A `bench.sh` _sbatch_ script is created, that automates this process. For this, modifications in the `main.c` were made, in order to be able to specify the linear algebra library to use, using compile-time _define_ directives (_OpenBLAS_ or _Intel MKL_). The `makefile` was modified in order to link the appropiate libraries, as well as to be able to change the optimization levels for each compiler. The results obtained are included in the file `dgesv-5366845.out`:

| Compiler | 128 | 256 | 512 |
| -------- | --- | --- | --- |
| gcc -O0  | 556 | 8672 | 125728 |
| gcc -O1  | 124 | 1933 | 29904 |
| gcc -O2  | 130 | 1856 | 27706 |
| gcc -O3  | 64 | 969 | 15913 |
| gcc -Ofast | 59 | 956 | 16940 |
| icx -O0 | 476 | 7564 | 119769 |
| icx -O3 | 55 | 832 | 13980 |
| icx -fast | 53 | 813 | 14074 |

As we can see, the time taken by the implementation is drastically reduced as more aggressive optimizations are introduced. The most aggresive one, `-Ofast`, might introduce precision errors in order to be faster, which might not be desirable depending on the application. However in this case, it even performs worse with higher matrix sizes. Unfortunately, I was not able to automate testing with the `icc` compiler, as I've run into a GLIBC library version error (only when using sbatch, which is very weird), so I only included the cases without optimization, and with the most aggresive ones, `-O3` and `-Ofast`. We can see `icx` is slightly faster overall. NOTE: I fixed this error later by loading the `cesga/2020` module instead of `cesga/2022`.
```shell
./dgesv: /lib64/libc.so.6: version `GLIBC_2.34' not found (required by ./dgesv)
```
These steps are on a separate commit tagged `task3-step2` on the repository.


### Task 3 and 4
_Try to achieve the compiler auto-vectorize the relevant parts of your solver._
_Analyze the performance obtained for your solver in the different runs of your benchmark. Pay special attention memory access patterns, cache and possible issues that are preventing the compiler to autovectorize your code._

In order to auto-vectorize, I first try to include the keyword `restrict` on the input matrices, and the intermediate buffers, which hints to the compiler that no other pointer will be used to access the same location. This allows the compiler to perform otherwise unsafe optimizations. No measurable time savings are observed with this move.

Another important thing, is to ensure the memory is aligned properly. The easiest way is to hint this to the compiler with *attributes*. We can therefore align variables to the cache line size in order to get better cache efficiency. This size can be checked with the following command, where a size of 64 is reported:
```shell
getconf -a | grep CACHE
```

```shell
LEVEL1_ICACHE_SIZE                 32768
LEVEL1_ICACHE_ASSOC                
LEVEL1_ICACHE_LINESIZE             64
LEVEL1_DCACHE_SIZE                 49152
LEVEL1_DCACHE_ASSOC                12
LEVEL1_DCACHE_LINESIZE             64
LEVEL2_CACHE_SIZE                  1310720
LEVEL2_CACHE_ASSOC                 20
LEVEL2_CACHE_LINESIZE              64
LEVEL3_CACHE_SIZE                  50331648
LEVEL3_CACHE_ASSOC                 12
LEVEL3_CACHE_LINESIZE              64
LEVEL4_CACHE_SIZE                  0
LEVEL4_CACHE_ASSOC                 
LEVEL4_CACHE_LINESIZE   
```

We can create a new type definition, and modify the appropiate internal arrays on the `dgesv` implementation.
```c
typedef double double_aligned __attribute__((aligned(64)));
```
However this only works with statically defined arrays. In this case I'm using heap memory allocated using `malloc`. For this reason, the `aligned_alloc` function was used instead of malloc. Unfortunately, it is only available on `C11` or greater. To ensure compatibility, I found that the function `posix_memalign` is preferred instead.

```c
	double *restrict x, *restrict m;
	if (posix_memalign((void *) &x, ALIGNMENT_SIZE, n * n * sizeof(double)) ||
		posix_memalign((void *) &m, ALIGNMENT_SIZE, n * mstride * sizeof(double))) {
		return -1;
	}
	// No calloc exists for aligned memory in the C standard, so we have to use memset
	memset(x, 0, n * n * sizeof(double));
```



Finally, the remaining options are the auto-vectorization and optimization compiler flags. Using the gcc option `-O3 -fopt-info-vec-all` we can get further information about what the compiler is able to vectorize:
```shell
dgesv.c:81:25: missed: couldn't vectorize loop
dgesv.c:81:25: missed: not vectorized: control flow in loop.
dgesv.c:83:22: missed: couldn't vectorize loop
dgesv.c:84:18: missed: not vectorized: no vectype for stmt: _72 = *_71;
 scalar_type: double_aligned
dgesv.c:68:21: missed: couldn't vectorize loop
dgesv.c:68:21: missed: not vectorized: multiple nested loops.
dgesv.c:69:22: missed: couldn't vectorize loop
dgesv.c:69:22: missed: not vectorized: control flow in loop.
dgesv.c:72:24: optimized: loop vectorized using 16 byte vectors
dgesv.c:72:24: optimized:  loop versioned for vectorization because of possible aliasing
dgesv.c:58:21: missed: couldn't vectorize loop
/mnt/netapp1/Optcesga_FT2_RHEL7/2022/gentoo/31032022/usr/include/bits/string_fortified.h:29:10: missed: statement clobbers memory: __builtin_memcpy (_15, _11, _7);
dgesv.c:22:5: note: vectorized 1 loops in function.
dgesv.c:51:31: missed: statement clobbers memory: x_119 = calloc (_2, 8);
dgesv.c:52:31: missed: statement clobbers memory: m_121 = malloc (_5);
/mnt/netapp1/Optcesga_FT2_RHEL7/2022/gentoo/31032022/usr/include/bits/string_fortified.h:29:10: missed: statement clobbers memory: __builtin_memcpy (_15, _11, _7);
/mnt/netapp1/Optcesga_FT2_RHEL7/2022/gentoo/31032022/usr/include/bits/string_fortified.h:29:10: missed: statement clobbers memory: __builtin_memcpy (b_122(D), x_119, _99);
dgesv.c:94:2: missed: statement clobbers memory: free (x_119);
dgesv.c:95:2: missed: statement clobbers memory: free (m_121);
dgesv.c:96:9: note: ***** Analysis failed with vector mode V2DF
dgesv.c:96:9: note: ***** Skipping vector mode V16QI, which would repeat the analysis for V2DF
```

Some loops cannot be vectorized because of the control flows. A rewrite of the code or a different algorithm may help to reduce those. There's one particular loop that can be optimized straight away:
```c
for (j = 0; j < n; j++) {
    for (i = 0; i < n; i++) {
        if (i > j)
        {
            c = m[i * mstride + j] / m[j * mstride + j];

            for (int k = 0; k <= n; k++)
                m[i * mstride + k] = m[i * mstride + k] - c * m[j * mstride + k];
        }
    }
}
```
It can be rewritten to remove the if, thus allowing the compiler to optimize it.
```c
for (j = 0; j < n; j++) {
    for (i = j+1; i < n; i++) {
            c = m[i * mstride + j] / m[j * mstride + j];
            for (k = 0; k <= n; k++) {
                m[i * mstride + k] -= c * m[j * mstride + k];
            }
    }
}
```

Finally, we can indicate to the compiler that no loop-carried dependencies on the inner loops with the `#pragma gcc ivdep` and  `#pragma ivdep` directives for `gcc` and `icx` respectively.

### Task 5
_Use Intel Advisor to improve the vectorization of your code. Which actions were recommended by Advisor, and in which parts of the code? How much performance gained after the vectorization was improved?_

First, we need to load the appropiate modules on a visualization node with remote desktop (or X-Forwarding): `module load cesga/2020 intel imkl advisor`. The program was compiled with `icx`, to ensure better compatibility with _Advisor_, and the options `-O3 -march=native -mtune=native`, which already includes vectorization, and optimizations for the instruction set supported by the processor.

![Captura de pantalla 2023-12-26 232946](img/Captura de pantalla 2023-12-26 232946.png)

![Captura de pantalla 2023-12-26 233009](img/Captura de pantalla 2023-12-26 233009.png)

![Captura de pantalla 2023-12-26 233009](img/Captura de pantalla 2023-12-26 234001.png)

![Captura de pantalla 2023-12-29 172318](C:\Users\vx\Documents\hpc\hpct\labs\task3\img\Captura de pantalla 2023-12-29 172318.png)

The advisor did not report any special observations, other than the loop is memory bound. Probably the much better performance obtained with the *lapack* implementations is achieved with a different, more efficient and more parallelizable algorithm, which splits the matrix on several smaller chunks, taking advantage of better cache locality.

### Task 6
_Parallelize your implementation of the routine dgesv using OpenMP. It is not required that you make a perfect parallelization, just, something parallel to work with. How did you parallelize your code? How much faster (speedup) is the parallelized code than the serial one?_

First, the program is compiled with the options `OMPCC=gcc CFLAGS="-O3 -march=native -mtune=native" make -j$(nproc)`. Each for loop that has no dependencies is parallelized. There's special directives like `#pragma omp simd` which informs the compiler it can attempt to use SIMD instructions for those loops. However it is not compatible together with the parallelization of for loops.

Upon the deadline extension, I took a look again at the entire code, and fixed some bugs that still remained, including small optimizations. For this reason the *git* repository does not reflect a linear, task by task history.

I've decided to include `#define` compile time directives in order to activate or deactivate selectively the main optimization steps. Those are compiler `#pragma` indications, with the `-DCC_OPT` flag, and memory optimizations, including memory alignment and optimized functions like `memcpy` or `memset`. This way the performance can be easily compared after the fact.

Finally, the *makefile* incorporates several options to be able to choose between different compilers (`gcc` with either *OpenBLAS* or *LaPACKe*, or `icx` with the *MKL* library).

These are automated on a batch script `bench.sh`, which retrieves all the results for the following configurations, using both compilers:
1. No optimizations at all (`-O0`).
2. All automatic compiler optimizations (`-O3`).
3. All optimizations for the current processor, plus manual `#pragma`, optimized `memcpy` functions and aligned memory (`-march=native -mtune=native -O3 -DCC_OPT -DFUNC_OPT`).
5. All optimizations mentioned above, plus *OpenMP* parallelization (`OMP_OPT=1 CFLAGS="-march=native -mtune=native -O3 -DCC_OPT -DFUNC_OPT"`).

The raw results can be found on the `dgesv-5391432.out` file:

|      Compiler/Size       |   32   |   64   |   128   |   256   |    512    |
| :----------------------: | :----: | :----: | :-----: | :-----: | :-------: |
|      1. GCC non-opt      |  2 ms  | 31 ms  | 475 ms  | 7523 ms | 119206 ms |
|      1. ICX non-opt      |  1 ms  | 24 ms  | 381 ms  | 6181 ms | 97270 ms  |
|  2. GCC automatic opts   |  0 ms  |  4 ms  |  53 ms  | 814 ms  | 13975 ms  |
|  2. ICX automatic opts   |  0 ms  |  3 ms  |  46 ms  | 694 ms  | 11713 ms  |
| 3. GCC all compiler opts |  0 ms  |  3 ms  |  46 ms  | 841 ms  | 13247 ms  |
| 3. ICX all compiler opts |  0 ms  |  2 ms  |  30 ms  | 495 ms  |  9153 ms  |
|      4. GCC OpenMP       | 141 ms | 181 ms | 355 ms  | 1904 ms | 15808 ms  |
|      4. ICX OpenMP       | 200 ms | 501 ms | 2178 ms | 3394 ms | 13950 ms  |

Here we can observe that the automatic performance optimizations, as well as the manual gains achieve consistent performance increases over each level. *ICX* nanages to gain some extra time over all circumstances over *GCC*. However the time taken exponentially increases with the size of the problem, and its not desirable. This does not happen on the libraries' `dgesv` implementation.  Further optimization of the code would likely require a new algorithm. The *OpenMP* time is actually worse, as the parallelization was not made efficiently, and a lot of `#pragma omp parallel` calls invoke and destroy threads multiple times for each column of the matrix, causing a lot of extra overhead. A more well-written optimized version that handles the creation of threads only on the first call of the function would be desirable, and probably will obtain faster times than the sequential version.



### Task 7

Finally, I take a look at the code with *Intel VTune*, which is a performance analysis and profiling tool. The objective is to locate possible *hotspots* and bottlenecks of the final parallelized code using the *hotspots* analysis.
In order to run it on the *Finis Terrae 3*, the following modules need to be loaded: `module load cesga/2020 intel imkl vtune`. On a virtual desktop session it can be launched with the command `vtune-gui`. Unfortunately, due to the *FT3's* permissions we cannot access the CPU's internal hardware counters, so we have to profile in User-Mode. For this analysis, the code was compiled with the command `CC=icx DEBUG=1 OMP_OPT=0 CFLAGS="-march=native -mtune=native -O3 -DCC_OPT -DFUNC_OPT" make -j$(nproc)`. *ICX* was chosen in order to ensure the best compatibility, and all of the optimization options are enabled, to evaluate which portions could be optimized further. *OpenMP* is not used, as the interactive visualization session is generated with only one core available.

![Captura de pantalla 2023-12-29 231735](C:\Users\vx\Documents\hpc\hpct\labs\task3\img\Captura de pantalla 2023-12-29 231735.png)

![Captura de pantalla 2023-12-29 231827](C:\Users\vx\Documents\hpc\hpct\labs\task3\img\Captura de pantalla 2023-12-29 231827.png)

![Captura de pantalla 2023-12-29 231935](C:\Users\vx\Documents\hpc\hpct\labs\task3\img\Captura de pantalla 2023-12-29 231935.png)

With this particular matrix size, `n=128`, the `my_dgesv` implementation is actually faster than the libraries' `dgesv`. However for bigger sizes, such as `n=512` or `n=1024` this difference quickly dissappears. This could probably be improved with an algorithm that divides the matrix in smaller chunks, and processes each chunk individually (or distributed on several threads). This is better for cache locality, as the access pattern is pretty complex and cannot fully saturate the compute performance of the processor. However, *Vtune* detected a *hotspot*, on a three-level nested loop, which unfortunately could not be parallelized with *OpenMP* due to its dependencies. Again, the access pattern could be modified in order to get more ordered accesses to memory, as well as trying to reduce the number of loops. Both the `j` and `i` loops shown as part of the hotspot on the screenshot could be collapsed into a single one, and probably parallelized using *SIMD* instructions.