# Lab AI - HPC Tools

## Motivation and initial ideas
In order to fulfill the purpose of the task, I started searching for possible models to use. By reviewing the examples, I've found that they use example datasets included as part of the pytorch (`https://pytorch.org/vision/stable/datasets.html`). In principle I thought of using one of these, **FGVC Aircraft**, because I like aviation and hoping no other students would use it. However, I quickly found out that this is only the dataset, and in order to perform the training it would be necessary to get the complete code, with the model and appropiate parameters, in order to be used for something useful.

Thus, I went on to the _Pytorch's examples_ repository and started searching through the examples. Browsing through the code, I chose the `vision_transformer` example, as it had a similar structure to the examples provided, so that it was easier to understand. It also used images, which is a recurrent and interesting topic on recent AI models. This particular one tries to perform image recongition, assigning to each image a category that represents what it contains (airplane, automobile, bird, cat, deer, dog, frog, horse, ship and truck).

## Baseline Implementation
I downloaded the example folder https://github.com/pytorch/examples/tree/main/vision_transformer and I tried to execute the training script `main.py` on an interactive node using torchrun. As it was taking too much time, I reduced the number of epochs from the default of 10 to just one. As the main focus of this lab is to learn how to parallelize, not how to properly train and validate the results of the model, I think this is OK.

Upon looking at the DDP examples, I tried first to incorporate this code into the model, as it provided a more "hands on" experience to what was actually happening, but quickly found it to be too difficult for my experience level.

Then, I started the parallelization used _PyTorch Lightning_, but I also encountered problems. Errors about missing attributes in objects, that upon searching for solutions, found no easy way to solve with this particular model. Probably due to some missing configuration on my part that I did not understand. I did include this attempt in the file `main_lightning.py`, although it does not work. One observation, is that while trying to execute it, it gave a suggestion about reducing the precision in order to use Tensor Cores on the GPU, which should speedup the process significantly. I added this to both the baseline implementation and the parallel one.
```
You are using a CUDA device ('NVIDIA A100') that has Tensor Cores.
To properly utilize them, you should set torch.set_float32_matmul_precision('medium' | 'high')
which will trade-off precision for performance.
For more details, read
https://pytorch.org/docs/stable/generated/torch.set_float32_matmul_precision.html#torch.set_float32_matmul_precision 
```
```python
torch.set_float32_matmul_precision('medium')
```

Finally, I decided to use Fabric, as I found the python interface easier to use. Here the adaptation was easy, just add the imports at the top of the file, add the Fabric initialization with the adequate parameters. I added arguments in order to choose different strategies, and matmul precisions. The only thing left was to prepare the shell scripts to launch them. `sbatch sbatch_baseline.sh` launches the baseline implementation.  `sbatch sbatch_2nodes.sh` launches all of the parallel implementations. Both scripts call `train.sh`, which launches the python script that handles the training.

I did run out of time at the end in order to test all of the implementations, however I was able to compare the baseline and the "automatic" fabric parallelization (before adding the strategy parameter, which uses the default one). It shows a reduction of almost 4 minutes, which is a reduction of 40% of the time. The outputs of these executions are on the files `baseline.out` and `fabric-auto.out` respectively.

| Implementation | Total job wall clock time |
|----------------|---------------------------|
| Baseline       |                  00:10:09 |
| Parallel Fabric (automatic strategy) | 00:06:08 |