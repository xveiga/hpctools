#!/bin/sh
#SBATCH --job-name=vision-transformer-fabric
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=2
#SBATCH --cpus-per-task=32
#SBATCH --mem=64G
#SBATCH --gres=gpu:a100:2
#SBATCH --time=00:30:00

# Available options: "dp", "ddp", "ddp_spawn", "xla", "deepspeed", "fsdp"
srun train.sh dp
srun train.sh ddp
srun train.sh ddp_spawn
srun train.sh xla
srun train.sh deepspeed
srun train.sh fsdp
