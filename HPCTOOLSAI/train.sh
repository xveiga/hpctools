#!/bin/bash

# My torchdist location is different so I had to change it
#source $STORE/conda/envs/mytorchdist/bin/activate
source $STORE/mytorchdist/bin/deactivate
source $STORE/mytorchdist/bin/activate

if [ $# -eq 0 ]; then
    echo "Baseline implementation"
    python main_baseline.py --epochs=1
    exit $!
elif [ $# -eq 1 ]; then
    echo "Fabric parallel implementation: $1"
    python main_fabric.py --epochs=1 --strategy="$1"
    exit $!
else
    echo "Wrong arguments. Usage: train.sh <strategy>"
    exit 1
fi